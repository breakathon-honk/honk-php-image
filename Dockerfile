ARG version
FROM php:${version}

RUN apt-get update && apt-get install -y unzip

## AWESOME Project : https://github.com/mlocati/docker-php-extension-installer
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/
RUN install-php-extensions zip intl opcache pdo pdo_mysql xml ldap mbstring gd curl redis

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

